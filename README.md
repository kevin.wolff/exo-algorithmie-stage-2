# Exercice algo stage 2

## Consignes

Vous devez mettre en place du  code pour que le personnage à l'écran puisse se déplacer, sur la droite, la gauche, et puisse sauter dans une direction voulue.

Vous commencerez sur papier par réflechir au principaux moyens à mettre en oeuvre (quelles fonctions? événements? ) pour produire un code fonctionnel, puis une fois fonctionnel, vous  transformerez votre code en une class "Personnage" qui une fois  instanciée, vous permettra de créer un personnage qui pourra être  déplacé.



## Réalisation

[x] Le personnage peut se déplacer à gauche ou à droite

[x] Le personnage peut sauter

[x] Lorsque le personnage saute sur une plateform, il attérit dessus

[x] Lorsque le personnage quitte une plateform, il déscend (fonctionne, travail en cours, voir console)

[x] Lorsque le personnage rencontre un obstacle (plateform, bord d'écran) il se déplace au plus prêt de celui-ci