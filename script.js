const screen = document.querySelector('#screen')

class game {
    constructor() {
        this.createCharacter()
        this.handlePlayerActions()
        this.createEnvironment()
    }

    // Init character.
    createCharacter() {
        this.character = document.createElement('div')
        this.character.classList.add('character')
        this.character.style.width = '30px'
        this.character.style.height = '30px'
        this.character.speed = 65
        this.character.jumpHeight = 130
        this.character.isJumping = false

        screen.append(this.character)
    }

    // Init environment.
    createEnvironment() {
        this.environment = screen
        this.environment.frame = {
            left: 0,
            bottom: 0,
            right: this.environment.scrollWidth,
            top: this.environment.scrollHeight
        }
        this.environment.obstacles = []
        this.environment.boxes = document.querySelectorAll('.box')
        this.environment.boxes.forEach(boxe => {
            this.environment.obstacles.push({
                left: boxe.offsetLeft,
                bottom: (boxe.offsetTop - boxe.scrollHeight),
                right: (boxe.offsetLeft + boxe.scrollWidth),
                top: boxe.offsetTop
            })
        })
    }

    // handle player actions.
    handlePlayerActions() {
        window.addEventListener('keydown', key => {
            switch (key.keyCode) {
                case key = 68:
                    this.handleMove('right')
                    break
                case key = 81:
                    this.handleMove('left')
                    break
                case key = 90:
                    this.handleMove('jump')
                    break
            }
        })
    }

    // Handle character movement.
    handleMove(action) {
        const frameBorder = this.handleFrameObstacles(action)
        const orderlyObstacles = this.handleOrderlyObstacles(action)

        if (action === 'right') {
            if (!frameBorder && !orderlyObstacles) {
                // Normal move.
                this.character.style.left = (this.character.offsetLeft + this.character.speed) + 'px'
            }
            if (frameBorder) {
                // Move close to frame border.
                this.character.style.left = frameBorder
            }
            if (orderlyObstacles) {
                // Move close to obstacle.
                this.character.style.left = orderlyObstacles
            }
        }

        if (action === 'left') {
            if (!frameBorder && !orderlyObstacles) {
                // Normal move.
                this.character.style.left = (this.character.offsetLeft - this.character.speed) + 'px'
            }
            if (frameBorder) {
                // Move close to frame border.
                this.character.style.left = frameBorder
            }
            if (orderlyObstacles) {
                // Move close to obstacle.
                this.character.style.left = orderlyObstacles
            }
        }

        if (action === 'jump') {
            this.handleJump()
        }
    }

    // Handle character jump.
    handleJump() {
        if (!this.character.isJumping) {
            this.character.isJumping = true
            this.character.style.top = (this.character.offsetTop - this.character.jumpHeight) + 'px'

            setTimeout(() => {
                const abscissaObstacle = this.handleAbscissaObstacles()

                if (!abscissaObstacle) {
                    this.character.style.top = (this.character.offsetTop + this.character.jumpHeight) + 'px'
                } else {
                    this.character.style.top = (abscissaObstacle - this.character.scrollHeight) + 'px'
                }

                this.character.isJumping = false
            }, 500)
        }
    }

    // Frame obstacle ? return distance between character and frame border || return false.
    handleFrameObstacles(action) {
        const remainDistRightFrameBorder = this.environment.frame.right - (this.character.offsetLeft + this.character.scrollWidth)
        const remainDistLeftFrameBorder = this.environment.frame.left + this.character.offsetLeft
        const remainDistTopFrameBorder = this.character.offsetTop


        if (action === 'right' && remainDistRightFrameBorder < this.character.speed) {
            return ((this.environment.frame.right - this.character.scrollWidth) + 'px')
        }

        if (action === 'left' && remainDistLeftFrameBorder < this.character.speed) {
            return (this.environment.frame.left + 'px')
        }

        if (action === 'jump' && remainDistTopFrameBorder < this.character.jumpHeight) {
            return true
        }

        return false
    }

    // Orderly obstacle ? return distance between character and obstacle || return false.
    handleOrderlyObstacles(action) {
        let orderlyObstacle

        if (action === 'right') {
            const offsetRight = this.character.offsetLeft + this.character.scrollWidth

            this.environment.obstacles.forEach(obstacle => {

                // If obstacle top if lower than character bottom there is no obstacle.
                if (obstacle.top > (this.character.offsetTop - this.character.scrollHeight)) {

                    if (obstacle.left < this.character.offsetLeft && obstacle.right > (this.character.offsetLeft + this.character.scrollWidth)) {
                        console.log('tu vas tomber à droite')
                    }

                    return false
                }

                // If obstacle is to close return appropriate next move.
                if ((obstacle.left - offsetRight) < this.character.speed && (obstacle.right - offsetRight) > this.character.speed) {
                    orderlyObstacle = ((obstacle.left - this.character.scrollWidth) + 'px')
                }
            })
        }

        if (action === 'left') {
            this.environment.obstacles.forEach(obstacle => {

                // If obstacle top if lower than character bottom there is no obstacle.
                if (obstacle.top > (this.character.offsetTop - this.character.scrollHeight)) {

                    if (obstacle.left < this.character.offsetLeft && obstacle.right > (this.character.offsetLeft + this.character.scrollWidth)) {
                        console.log('tu vas tomber à gauche')
                    }

                    return false
                }

                // If obstacle is to close return appropriate next move.
                if ((obstacle.right - this.character.offsetLeft) < this.character.speed) {
                    orderlyObstacle = (obstacle.right + 'px')
                }
            })
        }

        if (orderlyObstacle) return orderlyObstacle

        return false
    }

    // Abscissa obstacle ? return offsetTop of obstacle || return false.
    handleAbscissaObstacles() {
        let abscissaObstacle

        this.environment.obstacles.forEach(obstacle => {
            if (obstacle.left < this.character.offsetLeft && obstacle.right > (this.character.offsetLeft + this.character.scrollWidth)) {

                if ((this.character.offsetTop - this.character.scrollHeight) < obstacle.top) {
                    abscissaObstacle = obstacle.top
                } else {
                    return false
                }
            }
        })

        return abscissaObstacle
    }
}

new game()